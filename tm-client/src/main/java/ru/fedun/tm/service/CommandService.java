package ru.fedun.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @NotNull
    public List<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}
