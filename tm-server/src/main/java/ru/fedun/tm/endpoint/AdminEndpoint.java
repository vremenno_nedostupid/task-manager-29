package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.IAdminEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint implements IAdminEndpoint {

    private ServiceLocator serviceLocator;

    public AdminEndpoint() {
    }

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName,
            @WebParam(name = "lastName", partName = "last") @NotNull final String lastName,
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) throws Exception {
        serviceLocator.getUserService().create(login, password, firstName, middleName, lastName, email);
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName,
            @WebParam(name = "lastName", partName = "last") @NotNull final String lastName,
            @WebParam(name = "password", partName = "password") @NotNull final String password,
            @WebParam(name = "email", partName = "email") @NotNull final String email,
            @WebParam(name = "role", partName = "role") @NotNull final Role role
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, firstName, middleName, lastName, email, role);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

}
