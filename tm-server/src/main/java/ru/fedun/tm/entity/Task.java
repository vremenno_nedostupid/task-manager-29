package ru.fedun.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "app_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractEntity implements Serializable {

    @NotNull
    @Column(nullable = false, columnDefinition = "TINYTEXT")
    private String name;

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description;

    @NotNull
    private Date startDate;

    @NotNull
    private Date completeDate;

    @NotNull
    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    @ManyToOne
    private Project project;

    @NotNull
    @ManyToOne
    private User user;

}
