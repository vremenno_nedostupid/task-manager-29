package ru.fedun.tm.exception.incorrect;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class IncorrectCompleteDateException extends AbstractRuntimeException {

    private final static String message = "Error! Complete date is incorrect...";

    public IncorrectCompleteDateException() {
        super(message);
    }

}
