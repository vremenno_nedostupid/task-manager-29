package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IService<E> {

    void persist(@NotNull E e);

    void merge(@NotNull E e);

    void remove(@NotNull E e);

}
