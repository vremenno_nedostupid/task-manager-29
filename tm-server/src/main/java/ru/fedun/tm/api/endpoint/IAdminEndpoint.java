package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;

public interface IAdminEndpoint {

    void createUserWithEmail(
            @NotNull String login,
            @NotNull String firstName,
            @NotNull String middleName,
            @NotNull String lastName,
            @NotNull String password,
            @NotNull String email
    ) throws Exception;

    void createUserWithRole(
            @NotNull SessionDTO session,
            @NotNull String login,
            @NotNull String firstName,
            @NotNull String middleName,
            @NotNull String lastName,
            @NotNull String password,
            @NotNull String email,
            @NotNull Role role
    ) throws Exception;

    void lockUserByLogin(@NotNull SessionDTO session, @NotNull String login) throws Exception;

    void unlockUserByLogin(@NotNull SessionDTO session, @NotNull String login) throws Exception;

    void removeUserByLogin(@NotNull SessionDTO session, @NotNull String login) throws Exception;

    void removeUserById(@NotNull SessionDTO session, @NotNull String login) throws Exception;

}
